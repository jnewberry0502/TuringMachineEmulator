#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <unistd.h>

using namespace std;

const char *initial_state_string = "Initial";
const char *accept_state_string = "Accept";
const char *move_left_string = "<";
const char *move_right_string = ">";

const int MAX_MEMORY_SIZE = 500;
const int MAX_CHARS = 255;
const int MAX_STATES = 255;

char memory[MAX_MEMORY_SIZE];
int memory_change[MAX_STATES][MAX_CHARS];
int memory_next[MAX_STATES][MAX_CHARS];
int state_next[MAX_STATES][MAX_CHARS];

int state = 0;
int accept_state = 0;

void FillMemory(char NewValue)
{
    for (int i = 0; i < MAX_MEMORY_SIZE; i++)
    {
        memory[i] = NewValue;
    }
}

void FillIntArray(int *array, int size, int NewValue)
{
    for (int i = 0; i < size; i++)
    {
        array[i] = NewValue;
    }
}

void InitializeMemory(string NewString)
{
    for (int i = 0; i < NewString.length(); i++)
    {
        memory[i] = NewString.at(i);
    }
}

vector<string> GetLines(istream &input)
{
    vector<string> lines;
    string line;
    while (getline(input, line))
    {
        lines.push_back(line);
    }

    return lines;
}

std::vector<std::string> split(std::string strToSplit, char delimeter)
{
    std::stringstream ss(strToSplit);
    std::string item;
    std::vector<std::string> splittedStrings;
    while (std::getline(ss, item, delimeter))
    {
        splittedStrings.push_back(item);
    }
    return splittedStrings;
}

string ReplaceAll(string &in, char findC, char replaceC)
{
    std::replace(in.begin(), in.end(), findC, replaceC);
    return in;
}

string GetSplitIndex(const string &line, int index)
{
    std::vector<string> SplitLine = split(line, ' ');
    if (SplitLine.size() > 0)
    {
        return SplitLine.at(index);
    }
}

bool LoadProgram(string filename)
{
    ifstream input_program;
    input_program.open("program.tm");

    vector<string> lines = GetLines(input_program);

    for (int i = 0; i < lines.size(); i++)
    {
        string line = lines.at(i);
        line = ReplaceAll(line, ',', ' ');
        string key = GetSplitIndex(line, 0);

        if (key == initial_state_string)
        {
            string value = GetSplitIndex(line, 1);
            value = ReplaceAll(value, 'q', ' ');
            state = std::stoi(value);
        }
        else if (key == accept_state_string)
        {
            string value = GetSplitIndex(line, 1);
            value = ReplaceAll(value, 'q', ' ');
            accept_state = std::stoi(value);
        }
        else
        {
            string splitindex;
            splitindex = GetSplitIndex(line, 0);
            splitindex = ReplaceAll(splitindex, 'q', ' ');
            int CurrentState = stoi(splitindex);

            splitindex = GetSplitIndex(line, 1);
            char CurrentCharacter = splitindex.at(0);

            splitindex = GetSplitIndex(line, 2);
            splitindex = ReplaceAll(splitindex, 'q', ' ');
            int NextState = stoi(splitindex);

            splitindex = GetSplitIndex(line, 3);
            char NextCharacter = splitindex.at(0);

            splitindex = GetSplitIndex(line, 4);
            int NextMem = stoi(splitindex);

            memory_change[CurrentState][CurrentCharacter] = NextMem;
            memory_next[CurrentState][CurrentCharacter] = NextCharacter;
            state_next[CurrentState][CurrentCharacter] = NextState;
        }
    }

    return true;
}

void PrintMemory(char *memory)
{
    for (int i = 0; i < 100; i++)
    {
        cout << memory[i];
    }
    cout << "\n";
    cout.flush();
}

void PrintMemAddr(int addr)
{
    for (int i = 0; i < addr; i++)
    {
        cout << ' ';
    }
    cout << "^" << endl;
}

int main()
{
    for (int i = 0; i < MAX_STATES; i++)
    {
        FillIntArray(memory_change[i], MAX_CHARS, 0);
        FillIntArray(memory_next[i], MAX_CHARS, 0);
        FillIntArray(state_next[i], MAX_STATES, -1);
    }

    LoadProgram("program.tm");
    FillMemory('_');

    string memStr = "";
    cout << "Enter Starting Memory:\n";
    getline(cin, memStr);
    InitializeMemory(memStr);

    int curMemAddr = 0;

    int returnStatus = 0;

    while (true)
    {
        PrintMemory(memory);
        PrintMemAddr(curMemAddr);
        char CurrentCharacter = memory[curMemAddr];
        memory[curMemAddr] = memory_next[state][CurrentCharacter];
        curMemAddr += memory_change[state][CurrentCharacter];
        state = state_next[state][CurrentCharacter];

        if (state == -1)
        {
            returnStatus = -1;
            break;
        }
        if (state == accept_state)
        {
            break;
        }

        usleep(100000);
    }

    PrintMemory(memory);
    PrintMemAddr(curMemAddr);

    cout << "Press [enter] to exit..." << endl;
    cin.ignore();
    return returnStatus;
}