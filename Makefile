build:
	g++ main.cpp -g -std=c++11 -o TuringMachineSimulator.exe

buildwindows:
	x86_64-w64-mingw32-g++ main.cpp -g -std=c++11 -o TuringMachineSimulatorWin86_64.exe --static

run:
	./TuringMachineSimulator.exe

all:    build run
